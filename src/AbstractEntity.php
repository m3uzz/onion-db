<?php
/**
 * This file is part of Onion DB
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionDb
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-db
 */
declare (strict_types = 1);

namespace OnionDb;


abstract class AbstractEntity
{
	/**
	 * @var string
	 */
    protected $_sEntity;
	
	/**
	 * @var string
	 */
    protected $_sClass;
	
	/**
	 * @var \OnionDb\Driver\AbstractPDO
	 */
    protected $_oConnection = null;
	
	/**
	 * @var string
	 */
    protected $_sPk;
	
	/**
	 * @var array
	 */
    protected $_aFieldType = null;
	
	/**
	 * @var array
	 */
    protected $_aChanged = [];

    
	/**
	 * 
	 * @param array $paConf
	 */
	public function __construct (array $paConf = [])
	{
		$this->setDbConf($paConf);
	}
	
	
	/**
	 * 
	 * @param string $psProperty
	 * @param mixed $pmValue
	 */
	public function __set (string $psProperty, $pmValue)
	{
		if (property_exists($this, $psProperty))
		{
			$lsMethod = 'set' . ucfirst($psProperty);
				
			if (method_exists($this, $lsMethod))
			{
				$this->$lsMethod($pmValue);
			}
			else
			{
				$this->$psProperty = $pmValue;
				$this->_aChanged[$psProperty] = true;
			}
		}
	}
	
	
	/**
	 * 
	 * @param string $psVar
	 * @param mixed $pmValue
	 * @return \OnionDb\AbstractEntity
	 */
	public function set (string $psVar, $pmValue) : AbstractEntity
	{
		if (property_exists($this, $psVar))
		{
			$this->$psVar = $pmValue;
			$this->_aChanged[$psVar] = true;
		}
		
		return $this;
	}

	
	/**
	 * 
	 * @param string $psProperty
	 * @return mixed
	 */
	public function __get (string $psProperty)
	{
		return $this->get($psProperty);
	}
	
	
	/**
	 * 
	 * @param string $psProperty
	 * @return mixed
	 */
	public function get (string $psProperty)
	{
		if (property_exists($this, $psProperty))
		{
			$lsMethod = 'get' . ucfirst($psProperty);
			
			if (method_exists($this, $lsMethod))
			{
				return $this->$lsMethod();	
			}
			else
			{
				return $this->$psProperty;
			}
		}
	}	
	
	
	/**
	 * 
	 * @param array|null $paConf
	 * @throws \Exception
	 * @return \OnionDb\AbstractEntity
	 */
	public function setDbConf (?array $paConf) : AbstractEntity
	{
		if (is_array($paConf) && count($paConf) > 0)
		{
		    $lsDriverName = (isset($paConf['driver']) ? $paConf['driver'] : 'PDOMySql');
		    
		    if ($lsDriverName != null && class_exists("\\OnionDb\\Driver\\{$lsDriverName}", true))
		    {
		        $lsDriver = "\\OnionDb\\Driver\\{$lsDriverName}";
		        $this->_oConnection = new $lsDriver($paConf);
		    }
		    else 
		    {
		        throw new \Exception("Database driver '{$lsDriverName}' do not exists");
		    }
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @return bool
	 */
	public function hasError () : bool
	{
		return $this->_oConnection->hasError();
	}
	
	
	/**
	 * 
	 * @return string|null
	 */
	public function getErrorMsg () : ?string
	{
		return $this->_oConnection->getErrorMsg();
	}
	
	
	/**
	 *
	 * @return string|null
	 */
	public function getErrorCode () : ?string
	{
		return $this->_oConnection->getErrorCode();
	}
	
	
	/**
	 *
	 * @return array|null
	 */
	public function getError () : ?array
	{
		return $this->_oConnection->getError();
	}
	
	
	/**
	 * 
	 * @param array|object $pmData
	 * @return \OnionDb\AbstractEntity
	 */
	public function populate ($pmData) : AbstractEntity
	{
	    $laData = null;
	    
	    if (is_object($pmData))
	    {
	        $laData = $pmData->getArrayCopy();
	    }
	    elseif (is_array($pmData))
	    {
	        $laData = $pmData;
	    }
	    
	    if (is_array($laData))
	    {
	        foreach ($laData as $lsField => $lmValue)
	        {
	            if (property_exists($this, $lsField))
	            {
	                $this->$lsField = $lmValue;
	            }
	        }
	    }
	    
	    return $this;
	}
	
	
	/**
	 * 
	 * @return array
	 */
	public function getArrayCopy () : array
	{
		$laAllProperties = get_object_vars($this);
		
		if (is_array($laAllProperties))
		{
			foreach ($laAllProperties as $lsKey => $lmValue)
			{
				if (substr($lsKey, 0, 1) !== '_')
				{
					$laProperties[$lsKey] = $this->get($lsKey);
				}
			}
		}
		
		return $laProperties;
	}
	
	
	/**
	 * Return the whole object and its children as an array
	 *
	 * @return array
	 */
	public function toArray () : array
	{
		$laProperties = get_object_vars($this);
	
		if (is_array($laProperties))
		{
			foreach ($laProperties as $lsVar => $lmValue)
			{
				if (substr($lsVar, 0, 1) !== '_')
				{
					if (is_array($lmValue) && count($lmValue) != 0)
					{
						foreach ($lmValue as $lsId => $lmObj)
						{
							if (is_object($lmObj) && method_exists($lmObj, 'toArray'))
							{
								$laReturn[$lsVar][$lsId] = $lmObj->toArray();
							}
							else
							{
								$lmGet = $this->get($lsId);
								$laReturn[$lsVar][$lsId] = (!empty($lmGet) ? $lmGet : $lmObj);
							}
						}
					}
					elseif (is_object($lmValue) && method_exists($lmValue, 'toArray'))
					{
						$laReturn[$lsVar] = $lmValue->toArray();
					}
					else
					{
						$lmGet = $this->get($lsVar);						
						$laReturn[$lsVar] = (!empty($lmGet) ? $lmGet : $lmValue);
					}
				}
			}
		}
	
		return $laReturn;
	}
	
	
	/**
	 * 
	 * @param bool $pbForce
	 * @return \OnionDb\AbstractEntity
	 */
	public function getReflection (bool $pbForce = false) : AbstractEntity
	{
	    if ($this->_aFieldType === null || $pbForce == true)
	    {
    	    $loRC = new \ReflectionClass($this);
    	    $lsDoc = $loRC->getDocComment();
    	    
    	    if (empty($this->_sClass))
    	    {
                $this->_sClass = $loRC->getName();
    	    }
    	    
            if (empty($this->_sEntity) && preg_match('/\*[\s]*@table[\s]*=[\s]*(.*?)[\s]*\n/i', $lsDoc, $laEntityName))
            {
                $this->_sEntity = $laEntityName[1];
            }
    
            $laEntity = $this->getArrayCopy();
            	    
    		if (is_array($laEntity))
    	    {
    	        foreach ($laEntity as $lsField => $lmValue)
    	        {
    	            $loProperty = $loRC->getProperty($lsField);
    	            $lsDoc = $loProperty->getDocComment();
    	            
    	        	if (preg_match('/\*[\s]*@var[\s]*(.*?)[\s]*(PK)[\s]*\n/i', $lsDoc, $laResult))
    	            {
    	                if (empty($this->_sPk))
    	                {
    	                   $this->_sPk = $lsField;
    	                }
    	                
    	                $this->_aFieldType[$lsField] = $laResult[1];
    	            }
    	            elseif (preg_match('/\*[\s]*@var[\s]*(.*?)[\s]*\n/i', $lsDoc, $laResult))
    	            {
    	                $this->_aFieldType[$lsField] = $laResult[1];
    	            }
    	            elseif(preg_match('/^num.*$/', $lsField))
    	            {
    	                $this->_aFieldType[$lsField] = 'num';
    	            }
    	        	elseif (is_int($lmValue) || is_float($lmValue))
    	            {
    	        	    $this->_aFieldType[$lsField] = 'num';
    	            }
    	        }
    	    }
	    }
	    
	    return $this;
	}
	
	
	/**
	 * 
	 * @param string|int $pmId
	 * @return bool
	 */
	public function find ($pmId) : bool
	{
	    return $this->_oConnection->find($this, $pmId);
	}
	
	
	/**
	 * 
	 * @param string|null $psWhere
	 * @return bool
	 */
	public function findOneBy (?string $psWhere = null) : bool
	{
	    return $this->_oConnection->findOneBy($this, $psWhere);
	}	
	
	
	/**
	 * 
	 * @param string|null $psWhere
	 * @param int $pnOffset        	
	 * @param int $pnPage        	
	 * @param array|string $pmOrdField        	
	 * @param string|null $psOrder
	 * @param array|string $pmGroup       	
	 * @return array|null
	 */
    public function findBy (?string $psWhere = null, int $pnOffset = 0, int $pnPage = 0, $pmOrdField = null, ?string $psOrder = null, $pmGroup = null) : ?array
	{
        return $this->_oConnection->findBy($this, $psWhere, $pnOffset, $pnPage, $pmOrdField, $psOrder, $pmGroup);
	}	
	
	
	/**
	 * 
	 * @param bool $pbIgnore
	 * @return bool
	 */
	public function flush (bool $pbIgnore = true) : bool
	{
	    return $this->_oConnection->flush($this, $pbIgnore);
	}
	
	
	/**
	 * 
	 * @return bool
	 */
	public function flushUpdate () : bool
	{
	    if ($this->_oConnection->update($this))
	    {
	        $this->_aChanged = [];
            return true;
	    }
	    
	    return false;
	}
	
	
	/**
	 * 
	 * @return bool
	 */
	public function flushDelete () : bool
	{
	    if ($this->_oConnection->delete($this))
	    {
	         $this->ResetObject();
	         return true;
	    }
	    
	    return false;
	}	
	
	
	/**
	 * 
	 * @param string|null $psWhere
	 * @param int $pnLimit
	 * @return bool
	 */
	public function update (?string $psWhere = null, int $pnLimit = 1) : bool
	{
	    if ($this->_oConnection->createQueryUpdate($this, $psWhere, $pnLimit))
	    {
	        if ($this->_oConnection->execute())
	        {
	            $this->_aChanged = [];
	            
	            return true;
	        }
	    }
	    
	    return false;
	}
	
	
	/**
	 * 
	 * @param string|null $psWhere
	 * @param int $pnLimit
	 * @return bool
	 */
	public function delete (?string $psWhere = null, int $pnLimit = 1) : bool
	{
	    if ($this->_oConnection->createQueryDelete($this, $psWhere, $pnLimit))
	    {
	        if ($this->_oConnection->execute())
	        {
	            $this->ResetObject();
	            return true;
	        }
	    }
	    
	    return false;
	}
	
	
	/**
	 * 
	 */
    public function ResetObject() : void
    {
        foreach ($this as $lsKey => $lmValue)
        {
            unset($this->$lsKey);
        }
    }
}