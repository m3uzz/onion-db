<?php
/**
 * This file is part of Onion DB
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionDb
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-db
 */
declare (strict_types = 1);

namespace OnionDb;
use OnionDb\Driver\AbstractPDO;


abstract class AbstractRepository
{
	/**
	 * @var string
	 */
	protected $sEntity = 'OnionDb\AbstractEntity';
	
	/**
	 * @var \OnionDb\Driver\AbstractPDO
	 */
	protected $oConnection = null;
	
	
	/**
	 * 
	 * @param array $paConf
	 */
	public function __construct (array $paConf = [])
	{
		$this->setDbConf($paConf);
	}
	
	
	/**
	 * 
	 * @param array|null $paConf
	 * @throws \Exception
	 */
	public function setDbConf (?array $paConf) : AbstractRepository
	{
		if (is_array($paConf) && count($paConf) > 0)
		{
		    $lsDriverName = (isset($paConf['driver']) ? $paConf['driver'] : 'PDOMySql');
		    
			if ($lsDriverName == "MySQL")
			{
				$lsDriverName = "PDOMySql";
			}
			
		    if ($lsDriverName != null && class_exists("\\OnionDb\\Driver\\{$lsDriverName}", true))
		    {
		        $lsDriver = "\\OnionDb\\Driver\\{$lsDriverName}";
		        $this->oConnection = new $lsDriver($paConf);
		    }
		    else 
		    {
		        throw new \Exception("Database driver '{$lsDriverName}' do not exists");
		    }
		}
		
		return $this;
	}
	
	
	/**
	 *
	 * @return bool
	 */
	public function hasError () : bool
	{
		return $this->oConnection->hasError();
	}
	
	
	/**
	 * 
	 * @return string|null
	 */
	public function getErrorMsg () : ?string
	{
		return $this->oConnection->getErrorMsg();
	}
	
	
	/**
	 *
	 * @return string|null
	 */
	public function getErrorCode () : ?string
	{
		return $this->oConnection->getErrorCode();
	}
	
	
	/**
	 *
	 * @return array|null
	 */
	public function getError () : ?array
	{
		return $this->oConnection->getError();
	}
	
	
	/**
	 * 
	 * @param string $psClass
	 * @param array $paConf
	 * @return \OnionDb\AbstractEntity
	 */
	public function getEntity (string $psEntity, ?array $paConf = null) : AbstractEntity
	{
	   if ($paConf == null)
	   {
	       $paConf = $this->oConnection->get('aConf');
	   }
	   
	   $loEntity = new $psEntity($paConf);
	   
	   return $loEntity;
	}
	
	
	/**
	 * 
	 * @return \OnionDb\Driver\AbstractPDO
	 */
	public function getConnection () : AbstractPDO
	{
	    return $this->oConnection;
	}
	
	
	/**
	 * 
	 * @param \OnionDb\AbstractEntity $poEntity
	 * @return \OnionDb\AbstractEntity
	 */
	public function persist (AbstractEntity $poEntity) : AbstractEntity
	{
	    $poEntity->setDbConf($this->oConnection->get('aConf'));
	    
	    return $poEntity;
	}
	
	
	/**
	 *
	 * @param string|int|float|bool $pmValue
	 * @return string
	 */
	public function escapeString ($pmValue) : string
	{
	    return $this->oConnection->escapeString($pmValue);
	}
	
	
	/**
	 *
	 * @param string $psEntity        	
	 * @param string|null $psWhere        	
	 * @param array|string $pmFields 
	 * @param string $psJoin       	
	 * @param int $pnOffset        	
	 * @param int $pnPage        	
	 * @param array|string $pmOrdField        	
	 * @param string|null $psOrder
	 * @param array|string $pmGroup
	 * @param bool $pbDistinct
	 * @return string|null
	 */
	public function select (string $psEntity, ?string $psWhere = null, $pmFields = '*', string $psJoin = '', int $pnOffset = 0, int $pnPage = 0, $pmOrdField = null, ?string $psOrder = null, $pmGroup = null, bool $pbDistinct = false) : ?string
	{
	    return $this->oConnection->createQuerySelect($psEntity, $psWhere, $pmFields, $psJoin, $pnOffset, $pnPage, $pmOrdField, $psOrder, $pmGroup, $pbDistinct);
	}
	
	
	/**
	 *
	 * @param string $psQuery        	
	 * @param string $psEntity
	 * @param array|null $paConf	
	 * @return array|null
	 */
	public function queryExec (string $psQuery, string $psEntity = "", ?array $paConf = null) : ?array
	{
	    $this->oConnection->setQuery($psQuery);
	    
		return $this->oConnection->queryExec($psEntity, $paConf);
	}
	
	
	/**
	 * 
	 * @param string $psQuery
	 * @param array|null $paConf
	 * @return bool
	 */
	public function execute (string $psQuery, ?array $paConf = null) : bool
	{
		return $this->oConnection->execute($psQuery, $paConf);
	}
	
	
	/**
	 * 
	 * @param string $psQuery
	 * @param array|null $paConf
	 * @return bool
	 */
	public function update (string $psQuery, ?array $paConf = null) : bool
	{
		return $this->execute($psQuery, $paConf);
	}

	
	/**
	 * 
	 * @param string $psQuery
	 * @param array|null $paConf
	 * @return bool
	 */
	public function insert (string $psQuery, ?array $paConf = null) : bool
	{
		return $this->execute($psQuery, $paConf);
	}

	
	/**
	 *
	 * @param string $psQuery
	 * @param array|null $paConf
	 * @return bool
	 */
	public function create (string $psQuery, ?array $paConf = null) : bool
	{
		return $this->execute($psQuery, $paConf);
	}
	
	
	/**
	 *
	 * @param string $psEntity
	 * @param array|null $paConf
	 * @return array|null
	 */
	public function descEntity (string $psEntity, ?array $paConf = null) : ?array
	{
        return $this->oConnection->descEntity($psEntity, $paConf);	    
	} 

	
	/**
	 * 
	 * @param string $psEntity
	 * @param string|int $pmId
	 * @return \OnionDb\AbstractEntity|null
	 */
	public function find (string $psEntity, $pmId) : ?AbstractEntity
	{
	    $loEntity = $this->getEntity($psEntity);
	    
	    if ($loEntity->find($pmId))
	    {
	       return $loEntity;
	    }
	    
	    return null;
	}
	
	
	/**
	 * 
	 * @param string $psEntity
	 * @param string $psWhere
	 * @return \OnionDb\AbstractEntity|null
	 */
	public function findOneBy (string $psEntity, string $psWhere) : ?AbstractEntity
	{
	    $loEntity = $this->getEntity($psEntity);
	    
	    if ($loEntity->findOneBy($psWhere))
	    {
	       return $loEntity;
	    }
	    
	    return null;
	}
	
	
	/**
	 * 
	 * @param string $psEntity
	 * @param string|null $psWhere
	 * @param int $pnOffset        	
	 * @param int $pnPage        	
	 * @param array|string $pmOrdField        	
	 * @param string|null $psOrder
	 * @param array|string $pmGroup       	
	 * @return array|null
	 */
	public function findBy (string $psEntity, ?string $psWhere = null, int $pnOffset = 0, int $pnPage = 0, $pmOrdField = null, ?string $psOrder = null, $pmGroup = null) : ?array
	{
	    $loEntity = $this->getEntity($psEntity);
   
	    return $loEntity->findBy($psWhere, $pnOffset, $pnPage, $pmOrdField, $psOrder, $pmGroup);
	}
}