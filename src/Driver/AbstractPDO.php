<?php
/**
 * This file is part of Onion DB
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionDb
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-db
 */
declare (strict_types = 1);

namespace OnionDb\Driver;
use OnionDb\AbstractEntity;
use OnionLib\Debug;
use PDOStatement;

class AbstractPDO extends AbstractDriver
{
	/**
	 * @var string
	 */
    protected $sQuery = null;

	/**
	 * @var string
	 */
	protected $sDriver = 'mysql';

    
	/**
	 * 
	 * @param array $paConf
	 */
	public function __construct (array $paConf = [])
	{
		$this->setConf($paConf);
	}

	
	/**
	 * 
	 * @param string $psQuery
	 */
	public function setQuery (string $psQuery) : AbstractPDO
	{
	    $this->sQuery = $psQuery;
	    
	    return $this;
	}
	
	
	/**
	 * 
	 * @param array|null $paConf
	 */
	public function setConf (?array $paConf) : AbstractPDO
	{
		if (is_array($paConf) && count($paConf) > 0)
		{
			$this->aConf['driver'] = (isset($paConf['driver']) ? $paConf['driver'] : 'PDOMySql');		    
			$this->aConf['hostname'] = (isset($paConf['hostname']) ? $paConf['hostname'] : null);
			$this->aConf['username'] = (isset($paConf['username']) ? $paConf['username'] : null);
			$this->aConf['password'] = (isset($paConf['password']) ? $paConf['password'] : null);
			$this->aConf['database'] = (isset($paConf['database']) ? $paConf['database'] : null);
			$this->aConf['port'] = (isset($paConf['port']) ? $paConf['port'] : '3306');
			$this->aConf['charset'] = (isset($paConf['charset']) ? $paConf['charset'] : 'UTF8');
		}
		
		Debug::debug($this->aConf);
		
		return $this;
	}

	
	
	/**
	 * 
	 * @param array|null $paConf
	 * @return bool
	 */
	public function connect (?array $paConf = null) : bool
	{
		if ($paConf == null)
		{
			$paConf = $this->aConf;
		}
		
		$lsCon = "{$this->sDriver}:host={$paConf['hostname']};port={$paConf['port']};dbname={$paConf['database']};charset={$paConf['charset']}";
		$lsUser = $paConf['username'];
		$lsPass = $paConf['password'];
		
		Debug::debug([$lsCon, $lsUser, $lsPass]);
		
		try {
			$this->oCon = new \PDO($lsCon, $lsUser, $lsPass);
			//Debug::debug($this->oCon);
			
			return true;
		}
		catch (\PDOException $e) {
			$this->setError([$e->getCode(), $e->getMessage()]);
			
			return false;
		}
	}
	
	
	/**
	 *
	 * @param string|null $psQuery
	 * @param array|null $paConf
	 * @return bool
	 */
	public function execute (?string $psQuery = null, ?array $paConf = null) : bool
	{
	    $this->setQuery($psQuery);
	    
		Debug::debug("QUERY: " . $this->sQuery);
		
		if ($this->connect($paConf))
		{
			$loStantement = $this->prepare($this->sQuery);
			
			$lbReturn = $loStantement->execute();

			Debug::debug($lbReturn);

			if (!$lbReturn)
			{ 	
				$this->setError($loStantement->errorInfo());
			}
			
			$this->close();

			return $lbReturn;
		}
				
		return false;
	}	
	
	
	/**
	 *
	 * @param string $psEntity
	 * @param array|null $paConf
	 * @return array|null
	 */
	public function descEntity (string $psEntity, ?array $paConf = null) : ?array
	{
		$lsQuery = "DESC {$psEntity}";
		
		$this->setQuery($lsQuery);
		
		return $this->queryExec("", $paConf);
	}
	
	
	/**
	 *
	 * @param string $psEntity
	 * @param array|null $paConf  	
	 * @return array|null
	 */
	public function queryExec (string $psEntity = "", ?array $paConf = null) : ?array
	{
    	Debug::debug("QUERY: " . $this->sQuery);
		
		if ($this->connect($paConf))
		{
			$loStantement = $this->prepare($this->sQuery);
			
			$lbReturn = $loStantement->execute();
			
			if ($lbReturn)
			{ 
				if (!empty($psEntity))
				{
					$laResultSet = $loStantement->fetchAll(\PDO::FETCH_CLASS, $psEntity);
				}
				else 
				{
					$laResultSet = $loStantement->fetchAll();
				}
				
				Debug::debug($laResultSet);
			
				if (is_array($laResultSet) && count($laResultSet) > 0)
				{
					return $laResultSet;
				}

			    return [];
			}
			else
			{
				$this->setError($loStantement->errorInfo());
			}
			
			$this->close();
		}
				
		return null;
	}	
	
	
	/**
	 *
	 * @param string $psEntity        	
	 * @param string|null $psWhere        	
	 * @param array|string $pmFields 
	 * @param string $psJoin       	
	 * @param int $pnOffset        	
	 * @param int $pnPage        	
	 * @param array|string $pmOrdField        	
	 * @param string|null $psOrder
	 * @param array|string $pmGroup
	 * @param bool $pbDistinct
	 * @return string|null
	 */
	public function createQuerySelect (string $psEntity, ?string $psWhere = null, $pmFields = '*', string $psJoin = '', int $pnOffset = 0, int $pnPage = 0, $pmOrdField = null, ?string $psOrder = null, $pmGroup = null, bool $pbDistinct = false) : ?string
	{
		$pnOffset = $this->escapeString($pnOffset);
		$pnPage = $this->escapeString($pnPage);
		$psOrder = strtoupper($this->escapeString($psOrder));
		
		$lsFields = '';
		$lsGroup = '';
		$lsOrder = '';
		$lsLimit = '';

		if (is_array($pmOrdField))
		{
			$lsComma = "";
		
			foreach ($pmOrdField as $lsField => $lsOrd)
			{
				if ($lsOrd != "ASC" && $lsOrd != "DESC" && $lsOrd != "RAND")
				{
					$lsOrd = 'ASC';
				}
				elseif ($lsOrd == "RAND")
				{
					$lsOrd = 'rand()';
				}
				
				$lsField = $this->escapeString($lsField);
				
				$lsOrder .= "{$lsComma}{$lsField} {$lsOrd}";
				$lsComma = ", ";
			}
				
			if (!empty($lsOrder))
			{
				$lsOrder = "ORDER BY {$lsOrder}";
			}
		}
		elseif (is_string($pmOrdField) && !empty($pmOrdField))
		{
			if ($psOrder != "ASC" && $psOrder != "DESC" && $psOrder != "RAND")
			{
				$psOrder = 'ASC';
			}
			elseif ($psOrder == "RAND")
			{
				$psOrder = 'rand()';
			}
			
			$pmOrdField = $this->escapeString($pmOrdField);
			
			$lsOrder = "ORDER BY {$pmOrdField} {$psOrder}";
		}
		
		if ($pnOffset > 0)
		{
			if ($pnPage > 0)
			{
				$lsLimit = "LIMIT {$pnOffset} OFFSET {$pnPage}";
			}
			else
			{
				$lsLimit = "LIMIT {$pnOffset}";
			}
		}
		
		if (is_array($pmGroup))
		{
			$lsComma = "";
				
			foreach ($pmGroup as $lsField)
			{
				$lsGroup .= "{$lsComma}{$lsField}";
				$lsComma = ", ";
			}
			
			if (!empty($lsGroup))
			{
				$lsGroup = "GROUP BY {$lsGroup}";
			}
		}
		elseif(is_string($pmGroup) && !empty($pmGroup))
		{
			$lsGroup .= "GROUP BY $pmGroup";
		}
		
		if (is_array($pmFields))
		{
			$lsComma = "";
			
			foreach ($pmFields as $lsAlias => $lsField)
			{
				if (is_string($lsAlias))
				{
					$lsFields .= "{$lsComma}{$lsField} AS '{$lsAlias}'";
				}
				else
				{
					$lsFields .= "{$lsComma}{$lsField}";
				}
				
				$lsComma = ", ";
			}
		}
		else
		{
			$lsFields = '*';
		}
		
		if (!empty($psWhere) && !preg_match("/^AND /i", $psWhere))
		{
		    $psWhere = "AND {$psWhere}";
		}

		$lsDistinct = $pbDistinct ? 'DISTINCT ' : '';

   		$lsSql = "
    		SELECT {$lsDistinct}{$lsFields}
    		FROM {$psEntity}
    		{$psJoin}
    		WHERE true {$psWhere}
    		{$lsGroup}
    		{$lsOrder}
    		{$lsLimit}";
    		
    	$this->sQuery = preg_replace(("/WHERE true AND /i"), "WHERE ", $lsSql);
    	
    	return $this->sQuery;
	}	
	
	
    /**
     * 
     * @param \OnionDb\AbstractEntity $poEntity
     * @param bool $pbIgnore
     * @return bool
     */
	public function createQueryInsert (AbstractEntity $poEntity, bool $pbIgnore = false) : bool
	{
	    $lsFields = '';
	    $lsValues = '';
	    $lsComma = '';
	    $lsIgnore = '';
	    
	    if ($pbIgnore)
	    {
	        $lsIgnore = 'IGNORE';
	    }
	    
	    $poEntity->getReflection();
	    
	    $laEntity = $poEntity->getArrayCopy();
	    
	    if (is_array($laEntity))
	    {
	        foreach ($laEntity as $lsField => $lmValue)
	        {
	            if ($poEntity->get('_sPk') != $lsField || !empty($lmValue))
	            {
	                $laFieldType = $poEntity->get('_aFieldType');
	                
	                switch ($laFieldType[$lsField])
        	        {
        	            case 'num':
        	            case 'int':
        	            case 'decimal':
        	            case 'float':
        	            case 'integer':
        	               if (!empty($lmValue))
        	               {
        	                   $lsValues .= $lsComma . "{$lmValue}";
        	               }
        	               else 
        	               {
        	                   $lsValues .= $lsComma . "NULL";
        	               }
        	               break;
        	            default:
        	               $lsValues .= $lsComma . "'{$lmValue}'";
        	        }
        	            
        	        $lsFields .= $lsComma . "`{$lsField}`";
        	        $lsComma = ', ';
	            }	            
	        }
	    }
	    
	    $lsEntity = $poEntity->get('_sEntity');
	    
	    if (!empty($lsEntity))
	    {
   	        $this->sQuery = "INSERT {$lsIgnore} 
   	                          INTO `{$lsEntity}` 
   	                                ({$lsFields}) 
   	                          VALUES ({$lsValues})";
   	        
   	        return true;
	    }
	    
	    $this->setError(["1", "There is no way to get the table name!"]);
	    
        return false;
	}
	
	
	/**
	 * 
	 * @param \OnionDb\AbstractEntity $poEntity
	 * @param string|null $psWhere
	 * @param int $pnLimit
	 * @return bool
	 */
	public function createQueryUpdate (AbstractEntity $poEntity, ?string $psWhere = null, int $pnLimit = 1) : bool
	{
	    $lsPk = null;
	    $lsWhere = null;
	    $lsValues = '';
	    $lsComma = '';

	    $poEntity->getReflection();
	    
	    $laEntity = $poEntity->getArrayCopy();
	    
	    if (is_array($laEntity))
	    {
	        foreach ($laEntity as $lsField => $lmValue)
	        {
	            $laFieldType = $poEntity->get('_aFieldType');
	            
	            switch ($laFieldType[$lsField])
	            {
	                case 'num':
	                case 'int':
	                case 'decimal':
	                case 'float':
	                case 'integer':
	                   if (!empty($lmValue))
        	           {
        	               $lsFieldValue = "`{$lsField}` = {$lmValue}";
        	           }
        	           else 
        	           {
        	               $lsFieldValue = "`{$lsField}` = NULL";
        	           }	                    
	                   break;
	                default:
	                   $lsFieldValue = "`{$lsField}` = '{$lmValue}'";
	            }

	            $laChanged = $poEntity->get('_aChanged');
	            
	            if (isset($laChanged[$lsField]))
	            {
	                $lsValues .= $lsComma . $lsFieldValue;
	                $lsComma = ', ';
	            }
	            
	            if ($poEntity->get('_sPk') == $lsField)
	            {
	                $lsPk = $lsFieldValue;
	            }
	        }
	    }
	    
		if ($psWhere != null)
	    {
	        $lsWhere = $psWhere;
	    }
	    elseif ($lsPk != null)
	    {
            $lsWhere = $lsPk;
	    }
	    else 
	    {
    	    $this->setError(["2", "There is no where clause!"]);
    	    
    	    return false;
	    }

        if (empty($lsValues))
        {
            $this->setError(["0", "There is no values changed to update!"]);
    	    
    	    return true;
        }
        
	    $lsEntity = $poEntity->get('_sEntity');
	    
	    if (!empty($lsEntity))
	    {
   	        $this->sQuery = "UPDATE `{$lsEntity}` 
   	                          SET {$lsValues} 
   	                          WHERE {$lsWhere} 
   	                          LIMIT {$pnLimit}";
   	        
   	        return true;
        }
        
        $this->setError(["1", "There is no way to get the table name!"]);
        
        return false;
	}
	
	
	/**
	 * 
	 * @param \OnionDb\AbstractEntity $poEntity
	 * @param string|null $psWhere
	 * @param int $pnLimit
	 * @return bool
	 */
	public function createQueryDelete (AbstractEntity $poEntity, ?string $psWhere = null, int $pnLimit = 1) : bool
	{
		$lsPk = null;
	    $lsWhere = null;
	    
	    $poEntity->getReflection();

	    $laFieldType = $poEntity->get('_aFieldType');
	    
	    if (isset($laFieldType[$poEntity->get('_sPk')]))
	    {
            switch ($laFieldType[$poEntity->get('_sPk')])
            {
                case 'num':
                case 'int':
                case 'decimal':
                case 'float':
                case 'integer':
                    $lsPk = "`{$poEntity->get('_sPk')}` = {$poEntity->get($poEntity->get('_sPk'))}";
                    break;
                default:
                    $lsPk = "`{$poEntity->get('_sPk')}` = '{$poEntity->get($poEntity->get('_sPk'))}'";
            }
	    }
	    
		if ($psWhere != null)
	    {
	        $lsWhere = $psWhere;
	    }
	    elseif ($lsPk != null)
	    {
            $lsWhere = $lsPk;
	    }
		else 
	    {
	        $this->setError(["2", "There is no where clause!"]);
    	    
    	    return false;
	    }	    

	    $lsEntity = $poEntity->get('_sEntity');
	    
	    if (!empty($lsEntity))
        {
    	    $this->sQuery = "DELETE FROM `{$lsEntity}` 
    	                      WHERE {$lsWhere} 
    	                      LIMIT {$pnLimit}";
    	    
    	    return true;
        }

        $this->setError(["1", "There is no way to get the table name!"]);
	    
        return false;	    
	}
	
	
	/**
	 * 
	 * @param string|null $psQuery
	 * @return \PDOStatement|bool
	 */
	public function prepare (?string $psQuery = null)
	{
	    if ($psQuery == null)
	    {
	        $psQuery = $this->sQuery;
	    }
	    
	    //Debug::debug($psQuery);
	    
	    return $this->oCon->prepare($psQuery);
	}
	
	
	/**
	 * 
	 * @return string
	 */
	public function lastInsertId () : string
	{
	    return $this->oCon->lastInsertId();
	}
	
	
	/**
	 * 
	 */
	public function close () : void
	{
	    $this->oCon = null;
	}
	
	
	/**
	 * 
	 * @param \OnionDb\AbstractEntity $poEntity
	 * @param int|string $pmId
	 * @return bool
	 */
	public function find (AbstractEntity $poEntity, $pmId) : bool
	{
	    $lsWhere = null;
	    $poEntity->getReflection();
	    
	    $laFieldType = $poEntity->get('_aFieldType');
	    
		if (isset($laFieldType[$poEntity->get('_sPk')]))
	    {
            switch ($laFieldType[$poEntity->get('_sPk')])
            {
                case 'num':
                case 'int':
                case 'decimal':
                case 'float':
                case 'integer':
                    $lsWhere = "`{$poEntity->get('_sPk')}` = {$pmId}";
                    break;
                default:
                    $lsWhere = "`{$poEntity->get('_sPk')}` = '{$pmId}'";
            }
	    }
	    
        return $this->findOneBy($poEntity, $lsWhere);    
	}
	
	
	/**
	 * 
	 * @param \OnionDb\AbstractEntity $poEntity
	 * @param string|null $psWhere
	 * @return bool
	 */
	public function findOneBy (AbstractEntity $poEntity, ?string $psWhere = null) : bool
	{
	    $poEntity->getReflection();
   
	    $lsEntity = $poEntity->get('_sEntity');
	    
	    if (!empty($lsEntity))
        {
   	        $this->createQuerySelect($lsEntity, $psWhere, '*', '', 1);

    		if ($this->connect())
    		{
    			$loStantement = $this->prepare();
    			
    			$this->close();
    			
    			if ($loStantement->execute())
    			{ 
    				$laResultSet = $loStantement->fetchAll(\PDO::FETCH_ASSOC);
   			        Debug::debug($laResultSet);
   			        
    				if (isset($laResultSet[0]))
    				{
    					$poEntity->populate($laResultSet[0]);
    					
    					return true;
    				}
    				else 
    				{
    				    return false;
    				}
    			}
    			else
    			{
    				$this->setError($loStantement->errorInfo());
    			}
    		}
        }

        $this->setError(["1", "There is no way to get the table name!"]);
        
        return false;	    
	}	
	
	
	/**
	 * 
	 * @param \OnionDb\AbstractEntity $poEntity
	 * @param string|null $psWhere
	 * @param int $pnOffset        	
	 * @param int $pnPage        	
	 * @param mixed $pmOrdField        	
	 * @param string|null $psOrder
	 * @param mixed $pmGroup       	
	 * @return array|null
	 */
    public function findBy (AbstractEntity $poEntity, ?string $psWhere = null, int $pnOffset = 0, int $pnPage = 0, $pmOrdField = null, ?string $psOrder = null, $pmGroup = null) : ?array
	{
	    $poEntity->getReflection();
       
	    $lsEntity = $poEntity->get('_sEntity');
	    
	    if (!empty($lsEntity))
        {
   	        $this->createQuerySelect($lsEntity, $psWhere, '*', '', $pnOffset, $pnPage, $pmOrdField, $psOrder, $pmGroup);
   	        
   	        return $this->queryExec($poEntity->get('_sClass'));
        }

        $this->setError(["1", "There is no way to get the table name!"]);
        
        return null;
	}	
	
	
	/**
	 * 
	 * @param \OnionDb\AbstractEntity $poEntity
	 * @param bool $pbIgnore
	 * @return bool
	 */
	public function flush (AbstractEntity $poEntity, bool $pbIgnore = true) : bool
	{
	    if ($this->createQueryInsert($poEntity, true))
	    {
    	    if ($this->connect())
		    {
			    $loStantement = $this->prepare();
	
			    $lbReturn = $loStantement->execute();
			
			    if ($lbReturn)
			    {
			        Debug::debug("MySQL insert OK");
			        
			        $lmId = $this->lastInsertId();
                    $poEntity->set($poEntity->get('_sPk'), $lmId);
			    }
			    else
			    {
				    $this->setError($loStantement->errorInfo());
			    }
		    }
		}

		$this->close();
			
		return $lbReturn;
	}
	
	
	/**
	 * 
	 * @param \OnionDb\AbstractEntity $poEntity
	 * @return string
	 */	
	public function getWhere (AbstractEntity $poEntity) : string
	{
	    $poEntity->getReflection();
	    $lsField = $poEntity->get('_sPk');
	    $lmValue = $poEntity->get($lsField);
	    
		$laFieldType = $poEntity->get('_aFieldType');
	            
	    switch ($laFieldType[$lsField])
	    {
	        case 'num':
	        case 'int':
	        case 'decimal':
	        case 'float':
	        case 'integer':
	            if (!empty($lmValue))
        	    {
        	        $lsFieldValue = "`{$lsField}` = {$lmValue}";
        	    }
        	    else 
        	    {
        	        $lsFieldValue = "`{$lsField}` = NULL";
        	    }	                    
	            break;
	        default:
	            $lsFieldValue = "`{$lsField}` = '{$lmValue}'";
	    }
	    
	    return $lsFieldValue;
	}
	
	
	/**
	 * 
	 * @param \OnionDb\AbstractEntity $poEntity
	 * @return bool
	 */
	public function update (AbstractEntity $poEntity) : bool
	{
        $lsWhere = $this->getWhere($poEntity);
	    
	    if ($this->createQueryUpdate($poEntity, $lsWhere, 1))
	    {
    	    if ($this->connect())
		    {
			    $loStantement = $this->prepare();
	
			    $lbReturn = $loStantement->execute();
			
			    if ($lbReturn)
			    {
			        Debug::debug("MySQL update OK");
			    }
			    else
			    {
				    $this->setError($loStantement->errorInfo());
			    }
		    }
		}

		$this->close();
			
		return $lbReturn;
	}	
	
	
	/**
	 * 
	 * @param \OnionDb\AbstractEntity $poEntity
	 * @return bool
	 */
	public function delete (AbstractEntity $poEntity) : bool
	{
        $lsWhere = $this->getWhere($poEntity);
	    
	    if ($this->createQueryDelete($poEntity, $lsWhere, 1))
	    {
    	    if ($this->connect())
		    {
			    $loStantement = $this->prepare();
	
			    $lbReturn = $loStantement->execute();
			
			    if ($lbReturn)
			    {
			        Debug::debug("MySQL delete OK");
			    }
			    else
			    {
				    $this->setError($loStantement->errorInfo());
			    }
		    }
		}

		$this->close();
			
		return $lbReturn;
	}	
}