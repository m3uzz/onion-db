<?php
/**
 * This file is part of Onion DB
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionDb
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-db
 */
declare (strict_types = 1);

namespace OnionDb\Driver;
use OnionLib\Str;


abstract class AbstractDriver
{
	/**
	 * @var array
	 */
    protected $aConf = [];

	/**
	 * @var \PDO
	 */
	protected $oCon = null;
	
	/**
	 * @var array
	 */
    protected $aError = null;    
    
    
	/**
	 * 
	 * @param string $psProperty
	 * @return mixed
	 */
	public function __get (string $psProperty)
	{
		return $this->get($psProperty);
	}
	
	
	/**
	 * 
	 * @param string $psProperty
	 * @return mixed
	 */
	public function get (string $psProperty)
	{
		if (property_exists($this, $psProperty))
		{
			$lsMethod = 'get' . ucfirst($psProperty);
			
			if (method_exists($this, $lsMethod))
			{
				return $this->$lsMethod();	
			}
			else
			{
				return $this->$psProperty;
			}
		}
	}	
	
	
	/**
	 * 
	 * @param array $paError
	 */
	public function setError (array $paError) : AbstractDriver
	{
	    $this->aError = $paError;
	    
	    return $this;
	}
	
	
	/**
	 *
	 * @return bool
	 */
	public function hasError () : bool
	{
		if ($this->aError != null);
		{
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * 
	 * @return string|null
	 */
	public function getErrorMsg () : ?string
	{
		if (isset($this->aError[2]))
		{
			return $this->aError[2];
		}
		
		return null;
	}
	
	
	/**
	 *
	 * @return string|null
	 */
	public function getErrorCode () : ?string
	{
		if (isset($this->aError[1]))
		{
			return $this->aError[1];
		}
	
		return null;
	}
	
	
	/**
	 *
	 * @return array|null
	 */
	public function getError () : ?array
	{
		if (is_array($this->aError))
		{
			return $this->aError;
		}
	
		return null;
	}	
	
	
	/**
	 *
	 * @param string|int|float|bool $pmValue        	
	 * @return string
	 */
	public function escapeString ($pmValue) : string
	{		
		return Str::escapeString($pmValue);
	}	
}