<?php
/**
 * This file is part of Onion DB
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionDb
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-db
 */
declare (strict_types = 1);

namespace OnionDb\Driver;
use OnionLib\Debug;


//ToDo
abstract class MongoDB extends AbstractDriver
{
	/**
	 * @var \mongoClient
	 */
    protected $oDb = null;
	
	/**
	 * @var string
	 */
    protected $sQuery = null;
    
    
	/**
	 * 
	 * @param array $paConf
	 */
	public function __construct (array $paConf = [])
	{
		$this->setConf($paConf);
	}

	
	/**
	 * 
	 * @param string $psQuery
	 */
	public function setQuery (string $psQuery) : MongoDB
	{
	    $this->sQuery = $psQuery;
	    
	    return $this;
	}
	
	
	/**
	 * 
	 * @param array|null $paConf
	 */
	public function setConf (?array $paConf) : MongoDB
	{
		if (is_array($paConf) && count($paConf) > 0)
		{
			$this->aConf['driver'] = (isset($paConf['driver']) ? $paConf['driver'] : 'MongoDB');		    
			$this->aConf['hostname'] = (isset($paConf['hostname']) ? $paConf['hostname'] : null);
			$this->aConf['username'] = (isset($paConf['username']) ? $paConf['username'] : null);
			$this->aConf['password'] = (isset($paConf['password']) ? $paConf['password'] : null);
			$this->aConf['database'] = (isset($paConf['database']) ? $paConf['database'] : null);
			$this->aConf['port'] = (isset($paConf['port']) ? $paConf['port'] : '27017');
			$this->aConf['charset'] = (isset($paConf['charset']) ? $paConf['charset'] : 'UTF8');
			$this->aConf['write'] = (isset($paConf['write']) ? $paConf['write'] : 1);
			$this->aConf['journal'] = (isset($paConf['journal']) ? $paConf['journal'] : true);
			$this->aConf['wTimeout'] = (isset($paConf['wTimeout']) ? $paConf['wTimeout'] : 10000);
		}
		
		Debug::debug($this->aConf);
		
		return $this;
	}

	
	/**
	 * 
	 * @param array|null $paConf
	 * @return bool
	 */
	public function connect (?array $paConf = null) : bool
	{
		if ($paConf == null)
		{
			$paConf = $this->aConf;
		}
		
		$lsCon = "mongodb://{$paConf['hostname']}:{$paConf['port']}";
		$lsUser = $paConf['username'];
		$lsPass = $paConf['password'];
		$lsW = $paConf['write'];
		$lbJ = $paConf['journal'];
		$lnWTimeOut = $paConf['wTimeout'];
		
		Debug::debug([$lsCon, $lsUser, $lsPass]);
		
		//try {
			//$this->oCon = new \mongoClient($lsCon, ["connect"=>true, "username"=>$lsUser, "password"=>$lsPass, "w"=>$lsW, "journal"=>$lbJ, "wTimeoutMS"=>$lnWTimeOut]);
			//$this->oDb = $this->oCon->selectDB($paConf['database']);
			
			//Debug::debug($this->oCon);
			
			return true;
		//}
		//catch (MongoException $e) 
		//{
			//$this->setError([$e->getCode(), $e->getMessage()]);
			
			//return false;
		//}
	}
	
	
	/**
	 * 
	 * @param string $psEntity
	 * @return object collection
	 */
	public function getCollection (string $psEntity) : object
	{
	    return $this->oDb->selectCollection($psEntity);
	}
	
	
	/**
	 *
	 * @param string|null $psQuery
	 * @param array|null $paConf
	 * @return array|null
	 */
	public function execute (?string $psQuery = null, ?array $paConf = null) : ?array
	{
	    $this->setQuery($psQuery);
	    
	    return $this->queryExec('', $paConf);
	}	
	
	
	/**
	 *
	 * @param string $psEntity
	 * @param array|null $paConf
	 * @return array|null
	 */
	public function descEntity (string $psEntity, ?array $paConf = null) : ?array
	{
		$lsQuery = "";//"DESC {$psEntity}";
		
		$this->setQuery($lsQuery);
		
		return $this->queryExec("", $paConf);
	}
	
	
	/**
	 *
	 * @param string $psEntity
	 * @param array $paConf   	
	 * @return array|null
	 */
	public function queryExec (string $psEntity = "", array $paConf = null) : ?array
	{
    	Debug::debug("QUERY: " . $this->sQuery);
		
		if ($this->connect($paConf))
		{
			$loStantement = $this->oCon->prepare($this->sQuery);
			
			$lbReturn = $loStantement->execute();
			
			if ($lbReturn)
			{ 
				if (!empty($psEntity))
				{
					$laResultSet = $loStantement->fetchAll(\PDO::FETCH_CLASS, $psEntity);
				}
				else 
				{
					$laResultSet = $loStantement->fetchAll();
				}
				
				Debug::debug($laResultSet);
			
				if (is_array($laResultSet) && count($laResultSet) > 0)
				{
					return $laResultSet;
				}

			    return [];
			}
			else
			{
				$this->setError([$loStantement->errorInfo()]);
			}
			
			$this->close();
		}
				
		return null;
	}	
	
	
	/**
	 *
	 * @param string $psEntity        	
	 * @param string|null $psWhere        	
	 * @param array|string $pmFields 
	 * @param string $psJoin       	
	 * @param int $pnOffset        	
	 * @param int $pnPage        	
	 * @param array|string $pmOrdField        	
	 * @param string|null $psOrder
	 * @param array|string $pmGroup       	
	 * @return string
	 */
	public function createQuerySelect (string $psEntity, ?string $psWhere = null, $pmFields = '*', string $psJoin = '', int $pnOffset = 0, int $pnPage = 0, $pmOrdField = null, ?string $psOrder = null, $pmGroup = null) : string
	{
		$pnOffset = $this->escapeString($pnOffset);
		$pnPage = $this->escapeString($pnPage);
		$psOrder = strtoupper($this->escapeString($psOrder));
		
		$lsFields = '';
		$lsGroup = '';
		$lsOrder = '';
		$lsLimit = '';

		if (is_array($pmOrdField))
		{
			$lsComma = "";
		
			foreach ($pmOrdField as $lsField => $lsOrd)
			{
				if ($lsOrd == "DESC")
				{
					$lsOrd = '-1';
				}
				else
				{
					$lsOrd = '1';
				}
				
				$lsField = $this->escapeString($lsField);
				
				$lsOrder .= "{$lsComma}\"{$lsField}\":{$lsOrd}";
				$lsComma = ", ";
			}
		}
		elseif (is_string($pmOrdField) && !empty($pmOrdField))
		{
			if ($psOrder == "DESC")
			{
				$psOrder = '-1';
			}
			else
			{
				$psOrder = '1';
			}
			
			$pmOrdField = $this->escapeString($pmOrdField);
			
			$lsOrder = "\"{$pmOrdField}\":{$psOrder}";
		}
		
		if ($pnOffset > 0)
		{
			$lsLimit = $pnOffset;
		}
		
		if (is_array($pmGroup))
		{
			$lsComma = "";
				
			foreach ($pmGroup as $lsField)
			{
				$lsGroup .= "{$lsComma}\"{$lsField}\"";
				$lsComma = ", ";
			}
		}
		elseif(is_string($pmGroup) && !empty($pmGroup))
		{
			$lsGroup .= "\"{$pmGroup}\"";
		}
		
		if (is_array($pmFields))
		{
			$lsComma = "";
			
			foreach ($pmFields as $lsAlias => $lsField)
			{
				$lsFields .= "{$lsComma}\"{$lsField}\":1";
				$lsComma = ", ";
			}
		}
		else
		{
			$lsFields = '';
		}
		
   		$this->sQuery = "{
   		    \"action\":\"find\",
   		    \"collection\":\"{$psEntity}\",
    		\"fields\":{{$lsFields}},
    		\"criteria\":{{$psWhere}},
    		\"order\":{{$lsOrder}},
    		\"group\":[{$lsGroup}],
    		\"limit\":{$lsLimit},
    		\"page\":{$pnPage}
	    }";
    	
    	return $this->sQuery;
	}	
	
	
    /**
     * 
     * @param object $poEntity
     * @param bool $pbIgnore
     * @return bool
     */
	public function createQueryInsert (object $poEntity, bool $pbIgnore = false) : bool
	{
	    $lsFields = '';
	    $lsValues = '';
	    $lsComma = '';
	    $lsIgnore = '';
	    
	    if ($pbIgnore)
	    {
	        $lsIgnore = true;
	    }
	    
	    $poEntity->getReflection();
	    
	    $laEntity = $poEntity->getArrayCopy();
	    
	    if (is_array($laEntity))
	    {
	        foreach ($laEntity as $lsField => $lmValue)
	        {
	            if ($poEntity->get('_sPk') != $lsField || !empty($lmValue))
	            {
	                $laFieldType = $poEntity->get('_aFieldType');
	                
	                if (is_array($laFieldType))
	                {
    	                switch ($laFieldType[$lsField])
            	        {
            	            case 'num':
            	            case 'int':
            	            case 'decimal':
            	            case 'float':
            	            case 'integer':
            	               if (!empty($lmValue))
            	               {
            	                   $lsFields .= $lsComma . "\"{$lsField}\":{$lmValue}";
            	               }
            	               else 
            	               {
            	                   $lsFields .= $lsComma . "\"{$lsField}\":NULL";
            	               }
            	               break;
            	            default:
            	               $lsFields .= $lsComma . "\"{$lsField}\":\"{$lmValue}\"";
            	        }
	                }
	            }	            
	        }
	    }
	    
	    $lsEntity = $poEntity->get('_sEntity');
	    
	    if (!empty($lsEntity))
	    {
   	        $this->sQuery = "{
   	            \"action\":\"insert\",
   	            \"ignore\":{$lsIgnore}, 
   	            \"collection\":\"{$lsEntity}\", 
   	            \"document\":{{$lsFields}}
	        }"; 
   	        
   	        return true;
	    }
	    
	    $this->setError(["1", "There is no way to get the table name!"]);
	    
        return false;
	}
	
	
	/**
	 * 
	 * @param object $poEntity
	 * @param string|null $psWhere
	 * @param int $pnLimit
	 * @return bool
	 */
	public function createQueryUpdate (object $poEntity, ?string $psWhere = null, int $pnLimit = 1) : bool
	{
	    $lsPk = null;
	    $lsWhere = null;
	    $lsValues = '';
	    $lsComma = '';

	    $poEntity->getReflection();
	    
	    $laEntity = $poEntity->getArrayCopy();
	    
	    if (is_array($laEntity))
	    {
	        foreach ($laEntity as $lsField => $lmValue)
	        {
	            $laFieldType = $poEntity->get('_aFieldType');
	            
	            if (is_array($laFieldType))
	            {
    	            switch ($laFieldType[$lsField])
    	            {
    	                case 'num':
    	                case 'int':
    	                case 'decimal':
    	                case 'float':
    	                case 'integer':
    	                   if (!empty($lmValue))
            	           {
            	               $lsFieldValue = "`{$lsField}` = {$lmValue}";
            	           }
            	           else 
            	           {
            	               $lsFieldValue = "`{$lsField}` = NULL";
            	           }	                    
    	                   break;
    	                default:
    	                   $lsFieldValue = "`{$lsField}` = '{$lmValue}'";
    	            }
    
    	            $laChanged = $poEntity->get('_aChanged');
    	            
    	            if (isset($laChanged[$lsField]))
    	            {
    	                $lsValues .= $lsComma . $lsFieldValue;
    	                $lsComma = ', ';
    	            }
    	            
    	            if ($poEntity->get('_sPk') == $lsField)
    	            {
    	                $lsPk = $lsFieldValue;
    	            }
	            }
	        }
	    }
	    
		if ($psWhere != null)
	    {
	        $lsWhere = $psWhere;
	    }
	    elseif ($lsPk != null)
	    {
            $lsWhere = $lsPk;
	    }
	    else 
	    {
    	    $this->setError(["2", "There is no where clause!"]);
    	    
    	    return false;
	    }

        if (empty($lsValues))
        {
            $this->setError(["0", "There is no values changed to update!"]);
    	    
    	    return true;
        }
        
	    $lsEntity = $poEntity->get('_sEntity');
	    
	    if (!empty($lsEntity))
	    {
   	        $this->sQuery = "UPDATE `{$lsEntity}` 
   	                          SET {$lsValues} 
   	                          WHERE {$lsWhere} 
   	                          LIMIT {$pnLimit}";
   	        
   	        return true;
        }
        
        $this->setError(["1", "There is no way to get the table name!"]);
        
        return false;
	}
	
	
	/**
	 * 
	 * @param object $poEntity
	 * @param string|null $psWhere
	 * @param int $pnLimit
	 * @return bool
	 */
	public function createQueryDelete (object $poEntity, ?string $psWhere = null, int $pnLimit = 1) : bool
	{
	    return true;
		$lsPk = null;
	    $lsWhere = null;
	    
	    $poEntity->getReflection();

	    $laFieldType = $poEntity->get('_aFieldType');
	    
	    if (is_array($laFieldType))
	    {
    	    if (isset($laFieldType[$poEntity->get('_sPk')]))
    	    {
                switch ($laFieldType[$poEntity->get('_sPk')])
                {
                    case 'num':
                    case 'int':
                    case 'decimal':
                    case 'float':
                    case 'integer':
                        $lsPk = "{" . $poEntity->get('_sPk') . ":" . $poEntity->get($poEntity->get('_sPk')) . "}";
                        break;
                    default:
                        $lsPk = "{" . $poEntity->get('_sPk') . ":'" . $poEntity->get($poEntity->get('_sPk')) . "'}";
                }
    	    }
    	    
    		if ($psWhere != null)
    	    {
    	        $lsWhere = $psWhere;
    	    }
    	    elseif ($lsPk != null)
    	    {
                $lsWhere = $lsPk;
    	    }
    		else 
    	    {
    	        $this->setError(["2", "There is no where clause!"]);
        	    
        	    return false;
    	    }	    
    
    	    $lsEntity = $poEntity->get('_sEntity');
    	    
    	    if (!empty($lsEntity))
            {
        	    $this->sQuery = "DELETE FROM `{$lsEntity}` 
        	                      WHERE {$lsWhere} 
        	                      LIMIT {$pnLimit}";
        	    
        	    return true;
            }
    
            $this->setError(["1", "There is no way to get the table name!"]);
    	    
            return false;
	    }
	}
	
	
	/**
	 * 
	 */
	public function close () : void
	{
		//$this->oCon->close();
		$this->oCon = null;
	}
	
	
	/**
	 * 
	 * @param object $poEntity
	 * @param string|int $pmId
	 * @return bool
	 */
	public function find (object $poEntity, $pmId) : bool
	{
	    $poEntity->getReflection();
	    
	    $lsEntity = $poEntity->get('_sEntity');
	    
	    if (!empty($lsEntity))
        {
            $laWhere = ["id" => $pmId];
            
            if ($this->connect())
            {
                $laResultSet = $this->oDb->$lsEntity->findOne($laWhere);
                //Debug::display($laResultSet);
                
                if (is_array($laResultSet))
        		{
        			$poEntity->populate($laResultSet);
        			return true;
        		}
        		else 
        		{
        		    return false;
        		}
            }
        }
        
        $this->setError(["1", "There is no way to get the collection name!"]);
        
	    return false;
	}
	
	
	/**
	 * 
	 * @param object $poEntity
	 * @param string|null $psWhere
	 * @return bool
	 */
	public function findOneBy (object $poEntity, ?string $psWhere = null) : bool
	{
	    $poEntity->getReflection();
   
	    $lsEntity = $poEntity->get('_sEntity');
	    
	    if (!empty($lsEntity))
        {
   	        //$this->createQuerySelect($lsEntity, $psWhere, '*', '', 1);

            
    		if ($this->connect())
    		{
    		    $loWhere = json_encode(["_id"=>1], 0);
    		    Debug::display($loWhere);
    		    $laResultSet = $this->oDb->$lsEntity->findOne($loWhere);
    			//$loStantement = $this->prepare();
    			
    			$this->close();
    			
    			//if ($loStantement->execute())
    			//{ 
    				//$laResultSet = $loStantement->fetchAll(\PDO::FETCH_ASSOC);
   			        Debug::debug($laResultSet);
   			        
    				if (isset($laResultSet[0]))
    				{
    					$poEntity->populate($laResultSet[0]);
    					
    					return true;
    				}
    				else 
    				{
    				    return false;
    				}
    			//}
    			//else
    			//{
    			//	$this->setError($loStantement->errorInfo());
    			//}
    		}
        }

        $this->setError(["1", "There is no way to get the table name!"]);
        
        return false;	    
	}	
	
	
	/**
	 * 
	 * @param object $poEntity
	 * @param string|null $psWhere
	 * @param int $pnOffset        	
	 * @param int $pnPage        	
	 * @param array|string $pmOrdField        	
	 * @param string|null $psOrder
	 * @param array|string $pmGroup       	
	 * @return array|null
	 */
    public function findBy (object $poEntity, ?string $psWhere = null, int $pnOffset = 0, int $pnPage = 0, $pmOrdField = null, ?string $psOrder = null, $pmGroup = null) : array
	{
	    $poEntity->getReflection();
       
	    $lsEntity = $poEntity->get('_sEntity');
	    
	    if (!empty($lsEntity))
        {
   	        $this->createQuerySelect($lsEntity, $psWhere, '*', '', $pnOffset, $pnPage, $pmOrdField, $psOrder, $pmGroup);
   	        
   	        return $this->queryExec($poEntity->get('_sClass'));
        }

        $this->setError(["1", "There is no way to get the table name!"]);
        
        return null;
	}	
	
	
	/**
	 * 
	 * @param object $poEntity
	 * @param bool $pbIgnore
	 * @return bool
	 */
	public function flush (object $poEntity, bool $pbIgnore = true) : bool
	{
	    $lbReturn = false;
	    
	    $poEntity->getReflection();
	    
//	    if ($this->createQueryInsert($poEntity, true))
//	    {
    	    if ($this->connect())
		    {
		        $loCollection = $this->getCollection($poEntity->get('_sEntity'));
		        $laDocument = $poEntity->toArray();
		        
			    $laReturn = $loCollection->insert($laDocument, ["j"=>true, "w"=>"majority"]);

		        if (is_array($laReturn))
    		    {
        			if ($laReturn['ok'] === 1)
    			    {
    			        $lbReturn = true;
    			        Debug::debug("MongoDB insert OK");
    			        $laDocument = $loCollection->findOne($laDocument);
    			        $poEntity->populate($laDocument);
    			    }
    			    else
    			    {
    				    $this->setError(["MongoDB " . $laReturn['errmsg']]);
    			    }
    		    }
    		    else
    		    {
    		        $this->setError(["MongoDB insert error"]);
    		    }			    
		    }
//		}

		$this->close();
			
		return $lbReturn;
	}
	
	
	/**
	 * 
	 * @param object $poEntity
	 * @return array
	 */	
	public function getWhere (object $poEntity) : array
	{
	    $poEntity->getReflection();
	    $lsField = $poEntity->get('_sPk');
	    $lmValue = $poEntity->get($lsField);	    
        $laFieldValue = [$lsField => $lmValue];
		
	    return $laFieldValue;
	}
	
	
	/**
	 * 
	 * @param object $poEntity
	 * @return bool
	 */
	public function update (object $poEntity) : bool
	{
	    $lbReturn = false;
        
	    $laWhere = ["_id"=>$poEntity->get('_id'), "id"=>$poEntity->get('id')];
        
        $laUpdate = $poEntity->toArray();
	    
//	    if ($this->createQueryUpdate($poEntity, $lsWhere, 1))
//	    {
    	    if ($this->connect())
		    {
	            $loCollection = $this->getCollection($poEntity->get('_sEntity'));
		        
		        $laReturn = $loCollection->update($laWhere, $laUpdate, ["justOne"=>true]);

    		    if (is_array($laReturn))
    		    {
        			if ($laReturn['ok'] === 1 && $laReturn['n'] === 1)
    			    {
    			        Debug::debug("MongoDB update OK");
    			        $lbReturn = true;
    			    }
    			    else
    			    {
    				    $this->setError(["MongoDB " . $laReturn['errmsg']]);
    			    }
    		    }
    		    else
    		    {
    		        $this->setError(["MongoDB update error"]);
    		    }			    
		    }
//		}

		$this->close();
			
		return $lbReturn;
	}	
	
	
	/**
	 * 
	 * @param object $poEntity
	 * @return bool
	 */
	public function delete (object $poEntity) : bool
	{	    
	    $lbReturn = false;
	    
        $laWhere = $poEntity->toArray();
        
   	    if ($this->connect())
	    {
	        $loCollection = $this->getCollection($poEntity->get('_sEntity'));
	        
		    $laReturn = $loCollection->remove($laWhere, ["justOne"=>true]);
			
		    if (is_array($laReturn))
		    {
    		    if ($laReturn['ok'] === 1 && $laReturn['n'] === 1)
    		    {
    		        Debug::debug("MongoDB delete OK");
    		        $lbReturn = true;
    		    }
    		    else
    		    {
    			    $this->setError(["MongoDB " . $laReturn['errmsg']]);
    		    }
		    }
		    else
		    {
		        $this->setError(["MongoDB delete error"]);
		    }
	    }

		$this->close();
			
		return $lbReturn;
	}	
}